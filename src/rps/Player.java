/*	* Name: Durjoy Barua  
 	* Student ID: 040919612 
 	* Course & Section: CST8132 302 
 	* Assignment: Lab 5
 	* Date: Oct 26th, 2018 
 */

package rps;
/**
 *This class takes parameter and assign it into the method
 * @param name, lastMove, wins and ties represents continuously name of the player, last move of the player, count of wins of each player and ties of the game
 * @return different values on different method for each property.
 */
public abstract class Player {
	
	
	//instance variable for the Player class
	protected String name;
	protected HandSign lastMove;
	protected int wins;
	protected static int ties = 0;
	
	public Player(String name) {//default constructor that take name parameter and assign it to the name of this class and instantiate wins as 0
		this.name = name;
		wins = 0;
	}
	
	public String getName() {//getName method handles the name of the user
		return name;
	}
	
	public HandSign getlastMove() {//getlastMove method that handles the lastMove of the user
		return lastMove;
	}
	
	public int getWins() {//getWins method handles the wins of the player
		return wins;
	}
	
	public static int getTies() {//getTies method handle the ties of the player
		return ties;
	}
	
	public int win() {//win method increment win by 1
		return wins++;
	}
	
	public static int tie() {//tie method increment tie by 1
		return ties++;
	}
	
	public String toString() {//toString method returns the Players name and win numbers
		return ("Player: "+name+
				"\n"
				+ "Wins: "+wins);
	}
	
	public abstract boolean takeTurn();//abstract class takeTurn is required by those class which extends Player
}
