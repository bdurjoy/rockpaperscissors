/*	* Name: Durjoy Barua  
 	* Student ID: 040919612 
 	* Course & Section: CST8132 302 
 	* Assignment: Lab 5
 	* Date: Oct 26th, 2018 
 */

package rps;
/**
 * This class handles the main class and initialize the object
 * @param RockPaperScissors assigns Playable as an object of that class
 * @return
 */
public class PlayGame {

	public static void main(String[] args) {
		
		//Instantiate RockPaperScissors to the object
		RockPaperScissors Playable = new RockPaperScissors(3);
		
		Playable.play();
		Playable.displayResults();
		
	}

}
