/*	* Name: Durjoy Barua  
	* Student ID: 040919612 
	* Course & Section: CST8132 302 
	* Assignment: Lab 5
	* Date: Oct 26th, 2018 
*/

package rps;
/**
 * This class interact with human and handles the input from the Human 
 * @param in property from Scanner represented from the Game class
 * @return true
 */
import java.util.Scanner;

public class Human extends Player {
	
	//instance variable of Scanner class in created at protected
	protected Scanner in;
	
	public Human(Scanner index) {//default constructor the takes an index property and pass it to the super class Player
		super(index.next());
		
		in = index;
		
		//Question: How many Scanner objects exist in memory at this point?
		//--3
	}
	
	

public boolean takeTurn() {//take turn method that handles the interaction with user and returns true at the end of the mthod
		
		//What happens if the Human class does not implement the takeTurn method?
		//--The Human class will indicated error. Because it's extends Player class which contains abstract method takeTurn().
	
		//turn boolean variable instantiated as false
		boolean turn = false;
		
		while(turn == false){
			
			//This print line asks user to enter values and after that it assigns the value into the String userMove
			System.out.println(getName()+", it's your turn (Enter R for Rock, P for Paper, S for Scissors, or Q to quit): ");
			String userMove = in.next().toUpperCase().substring(0);
			
			switch(userMove) {//Switch statement that takes userMoves and assigns different duty for different situation
			case "Q":
				return false;
			case "S":
			case "P":
			case "R":
				//If user inputs S, P or R the lastMove of HandSign array takes the input and displays the name of the user and their moves
				lastMove = HandSign.getHandSign(userMove);
				System.out.println(getName()+" played: "+lastMove);
				turn = true;
			break;
				
			default:
				//The input of user is not valid when user input any other character than S, P or R
				System.out.println("Invalid input, please try again...");
				turn = false;
			break;
			}
		}
		
		return true;
	}
}
