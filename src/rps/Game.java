/*	* Name: Durjoy Barua  
 	* Student ID: 040919612 
 	* Course & Section: CST8132 302 
 	* Assignment: Lab 5
 	* Date: Oct 26th, 2018 
 */

package rps;
/**
 * Game class turns full program workable
 *@param numRounds, players and in represents continuously as Number of rounds, player numbers and Scanner property
 *@return none
 */
import java.util.Scanner;
import java.lang.Exception;


public abstract class Game implements Playable {
	
	//instance variable declaration which are assigned protected so that they can be used by other classes but stays hidden.
	protected int numRounds;
	protected Player[] players;
	protected Scanner in;
	
	
	public Game(int rounds) {//default constructor that instantiated the array players on the NUM_PLAYERS
		numRounds = rounds;
		
		
		players = new Player[NUM_PLAYERS];
		
		//Scanner class instantiated
		in = new Scanner(System.in);
		
		//initPlayers from playable interface accelerated th game
		initPlayers();
		
		
		//Question: How can the Game class invoke the method initPlayers without getting an error, if it has not yet been defined?
		//--Playable is an abstract interface. So when the Game class implements Playable initPlayers() method can be defined without accessing the class at first.
		
		}
	
	
	@Override
	public void finalize() {//finalize method tries to close the Scanner method and does nothing when it gets exception
		try {
			in.close();
		}catch(Exception e) {
			
		}
	}
}
