/*	* Name: Durjoy Barua  
 	* Student ID: 040919612 
 	* Course & Section: CST8132 302 
 	* Assignment: Lab 5
 	* Date: Oct 26th, 2018 
 */

package rps;
/**
 * This interface implements methods which are being used to other classes as abstract method
 * @param NUM_PLAYERS assigned int as value as 2
 * @return none
 */
public interface Playable {
	
	public int NUM_PLAYERS = 2;
	
	
	public void initPlayers() ;
	
	public void play() ;
	
	public void evaluateRound() ;
	
	public void displayResults() ;
	
	//The access level of this method is public. And they are concrete?
	//--Yes. They are concrete.
}
