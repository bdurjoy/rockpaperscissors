/*	* Name: Durjoy Barua  
	* Student ID: 040919612 
	* Course & Section: CST8132 302 
	* Assignment: Lab 5
	* Date: Oct 26th, 2018 
*/

package rps;
/**
 * Handles the sign of the computer randomly where it takes a parameter from random
 * @param "Hal" represents the name of the computer
 * @return Print the information about the computer moves and return true at the end of the program
 */
import java.util.Random;

public class Computer extends Player{
	
	//Default constructor computer
	public Computer() {
		//Pass HAL as a parameter to the super class.
		super("HAL");
	}
	
	//Question: Would it be possible to use the implicit default constructor of the computer class?
	//--Yes. It would be possible.
	

	public boolean takeTurn() {
		//Random clas declaration
		Random random = new Random();
		
		//Question: What type of variable is this ? What is the scope of this variable? Does it require Javadoc - why or why not?
		//--It's an int type variable. It gets it's value from PlayGame class. Yes it requires javadoc
		
		//random object that assigns 1 value from 3 int values
		lastMove = HandSign.values()[random.nextInt(3)];
		
		
		//The print line that indicate computer is playing
		System.out.println(getName()+", it's your turn!");
		System.out.println(getName()+", played "
				+getlastMove());
		
		return true;
	}
	
}
