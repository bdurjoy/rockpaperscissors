/*	* Name: Durjoy Barua  
 * Student ID: 040919612 
 * Course & Section: CST8132 302 
 * Assignment: Lab 5
 * Date: Oct 26th, 2018 
 */

package rps;
/**
 * This class extends game class and handles the logic of the game and assigns human moves and computer moves in an array.
 * @param numRounds as number of rounds passed to the game class
 * @return none
 */
public class RockPaperScissors extends Game {

	
	public RockPaperScissors(int numRounds) {//default constructor takes number of rounds and pass it to the super class Game
		super(numRounds);
	}


	public void initPlayers() {//initPlayers method that ask user to input their name and assigns it to players array and then instantiate computer moves with players array
		System.out.println("Enter your name: ");
		players[0] = new Human(in);
		players[1] = new Computer();
	}


	public void play() {//play method contains the loop to check the Human moves and as well to take the computer moves
		for(int i = 0; i < numRounds; i++) {
			if(players[0].takeTurn() == false) {
				break;
			}else {
				players[1].takeTurn();
			}

			evaluateRound();

		}
	}


	public void evaluateRound() {//evaluateRound method handles the evaluation of the game how it should operate
		
		//moves takes to parameters in it's array
		HandSign[] moves = {players[0].getlastMove(), players[1].getlastMove()};
		
		
		//winner parameter assigns the value from the return of the enum's getWinner methods
		int winner = HandSign.getWinner(moves);
		
		
		//
		if(winner == -1) {
			Player.tie();
			System.out.println("It's a tie! \nTies: "+Player.getTies());
		}else{
			
			int loser =  winner - 1;
			if(loser < 0) {
				loser = Math.abs(loser);

			}
			System.out.println(moves[winner]+
					" beats "
					+moves[loser]+"!");
			
			System.out.println(players[winner].getName()+
					" wins!");

			players[winner].win();
		}
	}


	public void displayResults() {//displayeResult method displays the result of the game at the end of the play

		Player winner = null;

		for(Player player: players) {//foreach loop that loop through the all players and assigns it to player
			
			player.toString();

			if(player.getWins() > (numRounds/2) ) {
				winner = player;
			}
		}

		System.out.println(Player.getTies());

		if(winner == null) {
			System.out.println("It's a tie game!");
		}else {
			System.out.println("The winner is "
					+winner.getName());
		}


		System.out.println("Thanks for playing Rock, Paper, Scissors. Bye!");

	}


}
